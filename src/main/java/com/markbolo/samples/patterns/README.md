## 设计模式
1. 状态模式(state):
2. 策略模式(strategy):
3. 单例模式(singleton):
    饿汉模式
    懒汉模式(simple)
    懒汉模式(synchronized)
    懒汉模式(LazyHolder)
    懒汉模式(Double Check)
    懒汉模式(Double Check + volatile)
    懒汉模式(Double Check + Thread Local) TODO
    
4. 工厂方法模式(factory method)
5. 抽象工厂模式(abstract factory)
6. Builder(构建器)模式
7. 原型模式(prototype)
8. 迭代器(Iterator)

