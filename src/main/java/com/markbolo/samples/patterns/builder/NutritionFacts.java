package com.markbolo.samples.patterns.builder;

/**
 * @author shenxf
 * @date 2017/8/31
 */
public class NutritionFacts {

    private int servingSize; // ml required
    private int servings; // per container  required
    private int calories; //  optional
    private int fat; // g  optional
    private int sodium; // mg  optional
    private int carbohydrate; // g  optional


    private NutritionFacts(Builder builder) {
        this.servings = builder.servings;
        this.servingSize = builder.servingSize;
        this.calories = builder.calories;
        this.fat = builder.fat;
        this.sodium = builder.sodium;
        this.carbohydrate = builder.carbohydrate;
    }

    public static class Builder {

        private int servingSize; // ml required
        private int servings; // per container  required

        private int calories = 0; //  optional
        private int fat = 0; // g  optional
        private int sodium = 0; // mg  optional
        private int carbohydrate = 0; // g  optional


        public Builder(int servingSize, int servings) {
            this.servingSize = servingSize;
            this.servings = servings;
        }

        public Builder calories(int val){
            this.calories = val;
            return this;
        }

        public Builder fat(int fat){
            this.fat = fat;
            return this;
        }

        public Builder sodium(int sodium){
            this.sodium = sodium;
            return this;
        }

        public Builder carbohydrate(int carbohydrate){
            this.carbohydrate = carbohydrate;
            return this;
        }

        public NutritionFacts build(){
            return new NutritionFacts(this);
        }
    }


}
