package com.markbolo.samples.patterns.builder.overlap;

/**
 * @author shenxf
 * @date 2017/8/31
 * Builder: 包装食品营养成分
 * 必要:
 *  每份的含量
 *  每罐的含量
 *  每份的卡路里
 * 可选:
 *  总脂肪量
 *  饱和脂肪量
 *  转化脂肪
 *  胆固醇
 *  纳...
 *
 *  重叠构造器模式
 */
public class NutritionFacts {

    private int servingSize; // ml required
    private int servings; // per container  required
    private int calories; //  optional
    private int fat; // g  optional
    private int sodium; // mg  optional
    private int carbohydrate; // g  optional


    public NutritionFacts(int servingSize, int servings) {
       this(servingSize,servings,0);
    }

    public NutritionFacts(int servingSize, int servings, int calories) {
        this(servingSize,servings,calories,0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat) {
        this(servingSize,servings,calories,fat,0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat, int sodium) {
       this(servingSize,servings,calories,fat,sodium,0);
    }

    public NutritionFacts(int servingSize, int servings, int calories, int fat, int sodium, int carbohydrate) {
        this.servingSize = servingSize;
        this.servings = servings;
        this.calories = calories;
        this.fat = fat;
        this.sodium = sodium;
        this.carbohydrate = carbohydrate;
    }
}
