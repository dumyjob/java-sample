package com.markbolo.samples.patterns.factory;

import com.markbolo.samples.patterns.factory.pizza.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class DependentPizzaStore {


    public Pizza createPizza(String type,String style) throws NoSuchPizzaException {

        Pizza pizza;

        if("NY".equals(style)){
            // New York Style Pizza
            if ("cheese".equals(type)) {
                pizza = new NewYorkStyleCheesePizza();
            } else if ("greek".equals(type)) {
                pizza = new NewYorkStyleGreekPizza();
            } else if ("pepperoni".equals(type)) {
                pizza = new NewYorkStylePepperoniPizza();
            } else if ("clam".equals(type)) {
                pizza = new NewYorkStyleClamPizza();
            } else if ("veggie".equals(type)) {
                pizza = new NewYorkStyleVeggiePizza();
            } else {
                throw new NoSuchPizzaException();
            }

        } else if("Chicago".equals(style)){
            // Chicago Style Pizza
            if ("cheese".equals(type)) {
                pizza = new ChicagoStyleCheesePizza();
            } else if ("greek".equals(type)) {
                pizza = new ChicagoStyleGreekPizza();
            } else if ("pepperoni".equals(type)) {
                pizza = new ChicagoStylePepperoniPizza();
            } else if ("clam".equals(type)) {
                pizza = new ChicagoStyleClamPizza();
            } else if ("veggie".equals(type)) {
                pizza = new ChicagoStyleVeggiePizza();
            } else {
                throw new NoSuchPizzaException();
            }
        } else if("California".equals(style)){
            // California Style Pizza
            if ("cheese".equals(type)) {
                pizza = new CaliforniaStyleCheesePizza();
            } else if ("greek".equals(type)) {
                pizza = new CaliforniaStyleGreekPizza();
            } else if ("pepperoni".equals(type)) {
                pizza = new CaliforniaStylePepperoniPizza();
            } else if ("clam".equals(type)) {
                pizza = new CaliforniaStyleClamPizza();
            } else if ("veggie".equals(type)) {
                pizza = new CaliforniaStyleVeggiePizza();
            } else {
                throw new NoSuchPizzaException();
            }
        } else {
            throw new NoSuchPizzaException();
        }

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }
}
