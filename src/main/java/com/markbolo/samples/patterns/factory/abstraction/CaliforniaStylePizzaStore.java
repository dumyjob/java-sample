package com.markbolo.samples.patterns.factory.abstraction;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class CaliforniaStylePizzaStore extends PizzaStore {

    @Override
    Pizza createPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;

        PizzaIngredientFactory ingredientFactory = new CalifornialPizzaIngredientFactory();

        if ("cheese".equals(type)) {
            pizza = new CheesePizza(ingredientFactory);
        } else if ("greek".equals(type)) {
            pizza = new GreekPizza(ingredientFactory);
        } else if ("pepperoni".equals(type)) {
            pizza = new PepperoniPizza(ingredientFactory);
        } else if ("clam".equals(type)) {
            pizza = new ClamPizza(ingredientFactory);
        } else if ("veggie".equals(type)) {
            pizza = new VeggiePizza(ingredientFactory);
        } else {
            throw new NoSuchPizzaException();
        }

        return pizza;
    }


    // usage:
    // PizzaStore pizzaStore = new CaliforniaStylePizzaStore();
    // pizzaStore.createPizza();
}
