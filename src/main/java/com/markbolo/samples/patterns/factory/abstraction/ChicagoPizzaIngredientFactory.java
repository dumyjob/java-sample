package com.markbolo.samples.patterns.factory.abstraction;

import com.markbolo.samples.patterns.factory.ingredient.*;
import com.markbolo.samples.patterns.factory.ingredient.chicago.FrozenClams;
import com.markbolo.samples.patterns.factory.ingredient.chicago.MozzarellaCheese;
import com.markbolo.samples.patterns.factory.ingredient.chicago.PlumTomSauce;
import com.markbolo.samples.patterns.factory.ingredient.chicago.ThickCrustDough;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomSauce();
    }

    @Override
    public Cheese createCheese() {
        return new MozzarellaCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[0];
    }

    @Override
    public Pepperoni createPepperoni() {
        return null;
    }

    @Override
    public Clams createClam() {
        return new FrozenClams();
    }
}
