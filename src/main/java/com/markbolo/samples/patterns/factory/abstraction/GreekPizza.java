package com.markbolo.samples.patterns.factory.abstraction;


/**
 * @author shenxf
 * @date 2017/9/4
 */
public class GreekPizza extends Pizza {

    private  PizzaIngredientFactory pizzaIngredientFactory;

    public GreekPizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    @Override
    void prepare() {
        System.out.println("Preparing " + getName());

        dough = pizzaIngredientFactory.createDough();
        sauce = pizzaIngredientFactory.createSauce();
        cheese = pizzaIngredientFactory.createCheese();
        clams = pizzaIngredientFactory.createClam();
    }
}
