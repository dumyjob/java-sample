package com.markbolo.samples.patterns.factory.abstraction;

import com.markbolo.samples.patterns.factory.ingredient.*;
import com.markbolo.samples.patterns.factory.ingredient.newyork.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class NewYorkPizzaIngredientFactory implements PizzaIngredientFactory{


    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public Cheese createCheese() {
        return new ReggianCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = new Veggies[]{
                new Garlic(),
                new Onion(),
                new Mushroom(),
                new RedPepper()
        };

        return veggies;
    }

    @Override
    public Pepperoni createPepperoni() {
        return new SlicedPepperoni();
    }

    @Override
    public Clams createClam() {
        return new FreshClams();
    }
}
