package com.markbolo.samples.patterns.factory.abstraction;

import com.markbolo.samples.patterns.factory.ingredient.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public interface PizzaIngredientFactory {

    Dough createDough();

    Sauce createSauce();

    Cheese createCheese();

    Veggies[] createVeggies();

    Pepperoni createPepperoni();

    Clams createClam();
}
