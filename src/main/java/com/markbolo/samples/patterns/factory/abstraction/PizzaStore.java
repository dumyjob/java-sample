package com.markbolo.samples.patterns.factory.abstraction;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public abstract class PizzaStore {


    public Pizza orderPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;

        pizza = createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }


    abstract Pizza createPizza(String type) throws NoSuchPizzaException;



}
