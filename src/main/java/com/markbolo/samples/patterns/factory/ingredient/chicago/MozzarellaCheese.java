package com.markbolo.samples.patterns.factory.ingredient.chicago;

import com.markbolo.samples.patterns.factory.ingredient.Cheese;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class MozzarellaCheese extends Cheese {
}
