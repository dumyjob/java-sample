package com.markbolo.samples.patterns.factory.method;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;
import com.markbolo.samples.patterns.factory.pizza.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class CaliforniaStylePizzaStore extends PizzaStore {

    @Override
    Pizza createPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;
        if ("cheese".equals(type)) {
            pizza = new CaliforniaStyleCheesePizza();
        } else if ("greek".equals(type)) {
            pizza = new CaliforniaStyleGreekPizza();
        } else if ("pepperoni".equals(type)) {
            pizza = new CaliforniaStylePepperoniPizza();
        } else if ("clam".equals(type)) {
            pizza = new CaliforniaStyleClamPizza();
        } else if ("veggie".equals(type)) {
            pizza = new CaliforniaStyleVeggiePizza();
        } else {
            throw new NoSuchPizzaException();
        }

        return pizza;
    }


    // usage:
    // PizzaStore pizzaStore = new CaliforniaStylePizzaStore();
    // pizzaStore.createPizza();
}
