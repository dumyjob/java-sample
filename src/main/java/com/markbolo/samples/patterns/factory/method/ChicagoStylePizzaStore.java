package com.markbolo.samples.patterns.factory.method;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;
import com.markbolo.samples.patterns.factory.pizza.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class ChicagoStylePizzaStore extends PizzaStore {

    @Override
    Pizza createPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;
        if ("cheese".equals(type)) {
            pizza = new ChicagoStyleCheesePizza();
        } else if ("greek".equals(type)) {
            pizza = new ChicagoStyleGreekPizza();
        } else if ("pepperoni".equals(type)) {
            pizza = new ChicagoStylePepperoniPizza();
        } else if ("clam".equals(type)) {
            pizza = new ChicagoStyleClamPizza();
        } else if ("veggie".equals(type)) {
            pizza = new ChicagoStyleVeggiePizza();
        } else {
            throw new NoSuchPizzaException();
        }

        return pizza;
    }

    // usage:
    // PizzaStore pizzaStore = new ChicagoPizzaStore();
    // pizzaStore.createPizza("veggie");
}
