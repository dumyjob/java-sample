package com.markbolo.samples.patterns.factory.method;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;
import com.markbolo.samples.patterns.factory.pizza.*;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class NewYorkStylePizzaStore extends PizzaStore {


    @Override
    Pizza createPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;
        if ("cheese".equals(type)) {
            pizza = new NewYorkStyleCheesePizza();
        } else if ("greek".equals(type)) {
            pizza = new NewYorkStyleGreekPizza();
        } else if ("pepperoni".equals(type)) {
            pizza = new NewYorkStylePepperoniPizza();
        } else if ("clam".equals(type)) {
            pizza = new NewYorkStyleClamPizza();
        } else if ("veggie".equals(type)) {
            pizza = new NewYorkStyleVeggiePizza();
        } else {
            throw new NoSuchPizzaException();
        }

        return pizza;
    }


    // usage:
    // PizzaStore pizzaStore = new NewYorkPizzaStore();
    // pizzaStore.createPizza("veggie");
}
