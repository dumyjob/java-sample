package com.markbolo.samples.patterns.factory.pizza;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class NewYorkStyleCheesePizza extends Pizza {

    public NewYorkStyleCheesePizza() {
        name = "NewYork Style Sauce and Cheese Pizza";
        dough = "Thin Crust Dough";
        sauce = "Marinara Sauce";

        toppings.add("Grated Reggiano Cheese");
    }
}
