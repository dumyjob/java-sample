package com.markbolo.samples.patterns.factory.pizza;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public abstract class Pizza {

    String name;

    String dough;

    String sauce;

    List<String> toppings = new ArrayList<String>();


    public void prepare() {

        System.out.println("Preparing " + getName());
        System.out.println("Tossing dough ......");
        System.out.println("Adding sauce ......");
        System.out.println("Adding toppings ......");

        for(String topping : toppings){
            System.out.println("   "+topping);
        }
    }

    public void bake(){
        System.out.println("Bake for 25 minutes at 350");
    }

    public void cut(){
        System.out.println("Cutting the pizza into diagonal slices");
    }

    public void box(){
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }
}
