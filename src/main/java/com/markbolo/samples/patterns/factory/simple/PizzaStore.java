package com.markbolo.samples.patterns.factory.simple;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;
import com.markbolo.samples.patterns.factory.pizza.Pizza;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class PizzaStore {

    private final SimpleFactory pizzaFactory;

    public PizzaStore(SimpleFactory pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    public Pizza orderPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;

        pizza = pizzaFactory.createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }


}
