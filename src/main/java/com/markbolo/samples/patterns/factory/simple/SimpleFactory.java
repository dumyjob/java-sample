package com.markbolo.samples.patterns.factory.simple;

import com.markbolo.samples.patterns.factory.NoSuchPizzaException;
import com.markbolo.samples.patterns.factory.pizza.*;

public class SimpleFactory {
    public SimpleFactory() {
    }

    Pizza createPizza(String type) throws NoSuchPizzaException {
        Pizza pizza;
        if ("cheese".equals(type)) {
            pizza = new CheesePizza();
        } else if ("greek".equals(type)) {
            pizza = new GreekPizza();
        } else if ("pepperoni".equals(type)) {
            pizza = new PepperoniPizza();
        } else if ("clam".equals(type)) {
            pizza = new ClamPizza();
        } else if ("veggie".equals(type)) {
            pizza = new VeggiePizza();
        } else {
            throw new NoSuchPizzaException();
        }

        return pizza;
    }
}