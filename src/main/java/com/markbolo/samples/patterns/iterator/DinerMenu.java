package com.markbolo.samples.patterns.iterator;

import java.util.Iterator;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class DinerMenu {

    static final int MAX_ITEMS = 6;

    int numberOfItems = 0;
    MenuItem[] menuItems;

    public DinerMenu() {
        menuItems = new MenuItem[MAX_ITEMS];

        addItem("Vegetarian BLT",
                "(Fakin') Bacon with lettuce & tomato on whole wheat",
                true,
                2.99);

    }

    private void addItem(String name, String description, boolean vegetarain, double price) {

        MenuItem menuItem = new MenuItem(name,description,vegetarain,price);
        if(numberOfItems >= MAX_ITEMS){
            System.out.println("Sorry, menu is full! Can't add item into menu");
        } else {
            menuItems[numberOfItems] = menuItem;
            numberOfItems ++;
        }
    }

    public Iterator iterator(){
        return new DinerMenuIterator(menuItems);
    }


    class DinerMenuIterator implements Iterator<MenuItem> {
        MenuItem[] items;
        int position = 0;

        public DinerMenuIterator(MenuItem[] items) {
            this.items = items;
        }

        @Override
        public boolean hasNext() {
            if(position >= items.length || items[position] == null){
                return false;
            } else {
                return true;
            }
        }

        @Override
        public MenuItem next() {
            MenuItem menuItem = items[position];
            position ++;

            return menuItem;
        }

        @Override
        public void remove() {

            if(position < 0){
                throw  new IllegalStateException("You can't remove an item until you've done at least ont next()");
            }

            if(items[position - 1] != null){
                for(int i = position -1;i<items.length;i++){
                    items[i] = items[i+1];
                }
                items[items.length-1] = null;
            }

        }
    }
}
