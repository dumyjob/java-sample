package com.markbolo.samples.patterns.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class PancakeHouseMenu {

    List<MenuItem> menuItems;

    public PancakeHouseMenu() {
        this.menuItems = new ArrayList<MenuItem>();

        addItem("K&B's Pancake Breakfast",
                "Pancakes with scrambled eggs,and toast",
                true,
                2.99);

        addItem("Regular Pancake Breakfast",
                "Pancakes with fried eggs,sauage",
                false,
                2.99);

        addItem("Blueberry Pancakes",
                "Pancakes made with fresh blueberries",
                true,
                3.49);

        addItem("Waffles",
                "Waffles, with you choice of blueberries or strawbeeries",
                true,
                3.59);
    }

    private void addItem(String name, String description, boolean vegetarian, double price) {

        menuItems.add(new MenuItem(name,
                description,
                vegetarian,
                price));
    }

    public Iterator iterator(){
        return menuItems.iterator();
    }



}
