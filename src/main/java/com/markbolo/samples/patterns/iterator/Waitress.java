package com.markbolo.samples.patterns.iterator;

import java.util.Iterator;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class Waitress {

    PancakeHouseMenu pancakeHouseMenu;
    DinerMenu dinerMenu;


    public Waitress(PancakeHouseMenu pancakeHouseMenu, DinerMenu dinerMenu) {
        this.pancakeHouseMenu = pancakeHouseMenu;
        this.dinerMenu = dinerMenu;
    }

    void printMenu(){

//        List<MenuItem> breakfastMenus = pancakeHouseMenu.getMenuItems();
//        MenuItem[] dinerMenus = dinerMenu.getMenuItems();
//
//        for(int i = 0 ;i < breakfastMenus.size();i++){
//            MenuItem menuItem = breakfastMenus.get(i);
//
//            System.out.println(menuItem.getName() + "");
//            System.out.println(menuItem.getDescription() + "");
//            System.out.println(menuItem.getPrice() + "");
//        }
//
//        for(int i = 0 ; i < dinerMenus.length ;i++){
//            MenuItem menuItem = dinerMenus[i];
//
//            System.out.println(menuItem.getName() + "");
//            System.out.println(menuItem.getDescription() + "");
//            System.out.println(menuItem.getPrice() + "");
//        }

        System.out.println("MENU\n----\n BREAKFAST");
        printMenu(pancakeHouseMenu.iterator());
        System.out.println("MENU\n----\n LAUNCH");
        printMenu(dinerMenu.iterator());
    }

    private void printMenu(Iterator iterator){
        for(Iterator<MenuItem> it = iterator;it.hasNext();){
            MenuItem menuItem =  it.next();

            System.out.println(menuItem.getName() + "");
            System.out.println(menuItem.getDescription() + "");
            System.out.println(menuItem.getPrice() + "");
        }

    }




}
