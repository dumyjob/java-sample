package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 * 双重检查锁定
 */
public class DoubleLock {

    private static DoubleLock instance;

    private DoubleLock() {
    }

    public static DoubleLock getInstance() {

        if(instance == null){
            synchronized (DoubleLock.class){
                if(instance == null){
                    instance = new DoubleLock();
                }
            }
        }

        return instance;
    }
}
