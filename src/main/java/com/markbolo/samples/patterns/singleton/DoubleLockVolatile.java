package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 */
public class DoubleLockVolatile {

    // 适用于jdk 1.5+
    private static volatile DoubleLockVolatile instance;

    private DoubleLockVolatile() {
    }

    public static DoubleLockVolatile getInstance() {

        if(instance == null){
            synchronized (DoubleLockVolatile.class){
                if (instance == null){
                    instance = new DoubleLockVolatile();
                }
            }
        }

        return instance;
    }
}
