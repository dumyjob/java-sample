package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 * 单例模式,Hungry
 * 线程安全
 */
public class Hungry {

    private static Hungry instance = new Hungry();

    private Hungry() {
    }

    public static Hungry getInstance(){
        return instance;
    }
}
