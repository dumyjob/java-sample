package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 * 单例模式,Lazy
 * 线程不安全,如何解决这个问题
 * 通过内部类的静态成员变量来解决线程安全问题
 *
 * Q: 是否能够实现延迟加载?
 * A: 能,内部类的静态成员变量只有在类初始化时候才被实例化,类初始化显然是在调用类以及类成员变量的时候初始化的.
 * 对于外部类来说,实例就是再getInstance()方法中才被实例化的,实现了延迟加载
 *
 * Q: 是否能保证线程安全,只实例化了一个外部类实例,而且所有方法获取的都是这一个实例
 * A: JVM保证了类只会加载一次,并且只会实例化一遍静态成员变量
 *
 */
public class InnerLazy {

    private static class LazyHolder {
        private static InnerLazy instance = new InnerLazy();
    }

    private InnerLazy() {
    }

    public static InnerLazy getInstance(){
        return LazyHolder.instance;
    }
}
