package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 * 单例模式,Lazy
 * 线程不安全,如何解决这个问题?
 */
public class Lazy {

    // 是否需要添加final修饰?
    private static Lazy instance;

    private  Lazy() {
    }

    public static Lazy getInstance(){
        if(instance == null){
            instance = new Lazy();
        }

        return instance;
    }
}
