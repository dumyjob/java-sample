package com.markbolo.samples.patterns.singleton;

/**
 * @author shenxf
 * @date 2017/8/31
 * 单例模式,Lazy
 * 线程不安全,如何解决这个问题
 * 在方法上加同步,在多线程情况下,每次都需要同步,同步比较消耗资源,对于大多数的情况都是不需要同步的
 * --只有第一次创建实例对象的时候才需要同步,避免第一次实例化多个实例
 */
public class SyncLazy {

    private  static  SyncLazy instance;

    private SyncLazy() {
    }

    public synchronized static SyncLazy getInstance(){
        if(instance == null){
            instance = new SyncLazy();
        }

        return instance;
    }
}
