package com.markbolo.samples.patterns.state;

import java.util.ArrayList;
import java.util.List;

/**
 * @auth Administrator
 * @date 2017/7/20
 * 客户类
 */
public class Customer {

    private String name;

    // TODO 为什么采用Vector? eg
    private List<Rental> rentals = new ArrayList<Rental>();

    public Customer(String name) {
        this.name = name;
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    public void addRental(Rental newRental) {
        rentals.add(newRental);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取打印详单的方法
     *
     * @return
     */
    public String statement() {
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");

        int frequentRenterPoints = 0;
        double totalAmount = 0;

        if (rentals != null && !rentals.isEmpty()) {

            for (Rental rental : rentals) {

                double thisAmount = 0;

                switch (rental.getMovie().getPriceCode()) {

                    case Movie.NORMAL:
                        thisAmount += 2;
                        if(rental.getDaysRented() > 2){
                            thisAmount += (rental.getDaysRented() -2) * 1.5;
                        }
                        break;
                    case Movie.CHILD:
                        thisAmount += rental.getDaysRented() * 3;
                        break;
                    case Movie.NEWER:
                        thisAmount += 1.5;
                        if(rental.getDaysRented() > 3){
                            thisAmount += (rental.getDaysRented() -3) *1.5;
                        }
                        break;
                }

                // add frequent renter points
                frequentRenterPoints ++;
                // add bonus for a two day new release rental
                if(rental.getMovie().getPriceCode() == Movie.NEWER && rental.getDaysRented() > 1){
                    frequentRenterPoints ++;
                }

                // show figures for this rental
                result.append("\t "+ rental.getMovie().getTitle() + "\t"+
                        String.valueOf(thisAmount) + "\n");

                totalAmount += thisAmount;
            }
        }


        // add footer lines
        result.append("Amount owed is " + String.valueOf(totalAmount) + "\n");
        result.append("You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points");

        return result.toString();
    }
}
