package com.markbolo.samples.patterns.state;

/**
 * @auth Administrator
 * @date 2017/7/20
 * 电影类,电影划分为普通片,儿童片,新片
 */
public class Movie {

    public static final int NORMAL = 0;
    public static final int CHILD = 1;
    public static final int NEWER = 2;

    private String title;

    private int priceCode;

    public Movie(String title, int priceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(int priceCode) {
        this.priceCode = priceCode;
    }
}
