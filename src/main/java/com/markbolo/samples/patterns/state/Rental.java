package com.markbolo.samples.patterns.state;

/**
 * @auth Administrator
 * @date 2017/7/20
 * 租赁类,表示客户租赁某影片多少天
 */
public class Rental {

    // 租赁多少天
    private Integer daysRented;
    // 租赁那部影片
    private Movie movie;

    public Integer getDaysRented() {
        return daysRented;
    }

    public void setDaysRented(Integer daysRented) {
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
