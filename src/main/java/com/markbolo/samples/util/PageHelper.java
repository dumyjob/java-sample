package com.markbolo.samples.util;

/**
 * @auth Administrator
 * @date 2017/7/25
 */
public class PageHelper {


    public static int start(final int pageIndex,final int pageSize){
        return pageIndex * pageSize + 1;
    }

    public static int end(final int pageIndex,final int pageSize,int totalCount){
        return (pageIndex + 1) * pageSize > totalCount ?  totalCount :  (pageIndex + 1) * pageSize;
    }

    public static int totalPage(final int totalCount,final int pageSize){
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }
}
