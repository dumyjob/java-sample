package com.markbolo.samples.excel;

import com.markbolo.samples.util.PageHelper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @auth Administrator
 * @date 2017/7/26
 */
public class ExcelTest {


    @Test
    public void multiSheetTest() {

        multiSheetWrite();
    }


    public void multiSheetWrite() {

        /**
         * 使用线程池进行线程管理。
         */
        ExecutorService es = Executors.newCachedThreadPool();
        /**
         * 使用计数栅栏
         */
        CountDownLatch doneSignal = new CountDownLatch(3);

        SXSSFWorkbook wb;
        try {
            wb = new SXSSFWorkbook(100);

            es.submit(new PoiMultiSheetWriter(doneSignal, wb.createSheet("0-19999"), 0, 19999));
            es.submit(new PoiMultiSheetWriter(doneSignal, wb.createSheet("20000-39999"), 20000, 39999));
            es.submit(new PoiMultiSheetWriter(doneSignal, wb.createSheet("40000-59999"), 40000, 59999));
            /**
             * 使用CountDownLatch的await方法，等待所有线程完成sheet操作
             */
            doneSignal.await();
            es.shutdown();

            FileOutputStream os = new FileOutputStream("E:\\test3Download.excel");
            wb.write(os);

            os.flush();
            os.close();
            System.out.println("Excel completed......");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected static class PoiMultiSheetWriter implements Runnable {

        private final CountDownLatch doneSignal;

        private Sheet sheet;

        private int start;

        private int end;

        public PoiMultiSheetWriter(CountDownLatch doneSignal, Sheet sheet, int start,
                                   int end) {
            this.doneSignal = doneSignal;
            this.sheet = sheet;
            this.start = start;
            this.end = end;
        }

        public void run() {
            int i = start;
            try {
                while (i <= end) {
                    Row row = sheet.createRow(i);
                    Cell contentCell = row.getCell(0);
                    if (contentCell == null) {
                        contentCell = row.createCell(0);
                    }
                    contentCell.setCellValue(i + 1);
                    ++i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                doneSignal.countDown();
                System.out.println("start: " + start + " end: " + end
                        + " Count: " + doneSignal.getCount());
            }
        }

    }


    @Test
    public void SingleSheetTest() {
        // List.addAll(index,subList)也是一个考虑方案
        singleSheetMultiThreadWrite();
    }


    /**
     * sheet的row使用treeMap存储的，是非线程安全的，所以在创建row时需要进行同步操作。
     *
     * @param sheet
     * @param rowNum
     * @return
     */
    private static synchronized Row getRow(Sheet sheet, int rowNum) {
        System.out.println("第" + rowNum + "行");
        return sheet.createRow(rowNum);
    }


    public void singleSheetMultiThreadWrite() {
        /**
         * 使用线程池进行线程管理。
         */
        ExecutorService es = Executors.newCachedThreadPool();
        /**
         * 使用计数栅栏
         */
        CountDownLatch doneSignal = new CountDownLatch(3);

        SXSSFWorkbook wb;
        try {
            wb = new SXSSFWorkbook(100);
            // xls 65536 excel 1048576
            Sheet singleSheet = wb.createSheet("Single Sheet");
            es.submit(new PoiSingleSheetWriter(doneSignal, singleSheet, 0, 99));
            es.submit(new PoiSingleSheetWriter(doneSignal, singleSheet, 100, 199));
            es.submit(new PoiSingleSheetWriter(doneSignal, singleSheet, 200, 299));
            /**
             * 使用CountDownLatch的await方法，等待所有线程完成sheet操作
             */
            doneSignal.await();
            es.shutdown();

            FileOutputStream os = new FileOutputStream("E:\\test1Download.excel");
            wb.write(os);

            os.flush();
            os.close();
            System.out.println("Excel completed......");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected static class PoiSingleSheetWriter implements Runnable {

        private final CountDownLatch doneSignal;

        private Sheet sheet;

        private int start;

        private int end;

        public PoiSingleSheetWriter(CountDownLatch doneSignal, Sheet sheet, int start,
                                    int end) {
            this.doneSignal = doneSignal;
            this.sheet = sheet;
            this.start = start;
            this.end = end;
        }

        public void run() {
            int i = start;
            try {
                while (i <= end) {
                    Row row = getRow(sheet, i);
                    Cell contentCell = row.getCell(0);
                    if (contentCell == null) {
                        contentCell = row.createCell(0);
                    }
                    contentCell.setCellValue(i + 1);
                    ++i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                doneSignal.countDown();
                System.out.println("start: " + start + " end: " + end
                        + " Count: " + doneSignal.getCount());
            }
        }
    }


    @Test
    public void testAddALL() {
        List<String> result1 = new ArrayList<String>();
        List<String> result2 = new ArrayList<String>();


        List<String> strings1 = new ArrayList<String>();
        strings1.add("1");
        strings1.add("2");
        strings1.add("3");
        strings1.add("4");

        List<String> strings2 = new ArrayList<String>();
        strings2.add("5");
        strings2.add("6");
        strings2.add("7");
        strings2.add("8");


        List<String> strings3 = new ArrayList<String>();
        strings3.add("9");
        strings3.add("10");
        strings3.add("11");
        strings3.add("12");


        result1.addAll(strings1);
        result1.addAll(strings2);
        result1.addAll(strings3);

        result2.addAll(0, strings1);
        result2.addAll(4, strings2);
        result2.addAll(8, strings3);

        Assert.assertEquals(result1, result2);
    }


    @Test
    public void testTwoPhase() {

        //TODO 考虑是否可以采用多线程的Two-Phase模式实现 -- Producer-Consumer模式
    }

    @Test
    public void testIsRowOrder() {

        // 出现不可读取内容和写入row的顺序没有任何关系
        // 是有关系的,和SXSSF模式有关系,如果是乱序写入,最好是采用-1,暂时还没弄的太清楚是什么关系
        SXSSFWorkbook wb = new SXSSFWorkbook(-1);
        // xls 65536 excel 1048576
        Sheet sheet = wb.createSheet("Single Sheet");

        for(int i = 2000 ; i > 1500 ; i--){
            sheet.createRow(i).createCell(0).setCellValue("row "+i);
        }
        for( int i = 1500 ; i >= 0 ;i--){
            sheet.createRow(i).createCell(0).setCellValue("row "+i);
        }


//        sheet.createRow(100).createCell(0).setCellValue("row 100");
//        sheet.createRow(10).createCell(0).setCellValue("row 10");


        try {
            FileOutputStream os = new FileOutputStream("E:\\testRowOrder.excel");
            wb.write(os);
            os.close();

            System.out.println("Excel completed......");
        } catch (IOException e) {
            e.printStackTrace();
        }
        // dispose of temporary files backing this workbook on disk
        wb.dispose();
    }


    @Test
    public void testProducerConsumer(){

        // test main线程
        // 生产者 从库中分页查询数据 放到ConcurrentHashMap (ps:放到hashMap中是为了保存顺序),ConcurrentHashMap也是线程安全的,不需要自己控制线程安全
        // 消费者 从ConcurrentHashMap取出数据,并写入到workbook.sheet中
        // 消费者一直监听ConcurrentHashMap没有数据而且生产者也停止生产后,则停止消费者线程
        // 多生产者,用来执行多个分页查询; 单个消费者,用来向workbook中写入数据,避免多线程同时写出现不可读取内容个错误
        // ?: 消费者如何监听多生产者 CountDownLatch只作为信号量是否可以?



        // 10个分页,每个分页1000条
        int pages = 10;
        final int subSize = 1000;

        final ConcurrentHashMap<Integer,List<String>> warehouse = new ConcurrentHashMap<Integer, List<String>>();
        final ExecutorService producerPools = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final CountDownLatch mainWatch = new CountDownLatch(pages+1); // pages - producer's , 1 - consumer
        final CountDownLatch producerWatch = new CountDownLatch(pages); // 仅作为producer和consumer之间的信号量


        // start producer
        for(int i =0 ;i < pages ; i++){

            final int startIndex = PageHelper.start(i,subSize);
            producerPools.execute(new Runnable() {

                @Override
                public void run() {

                    // 模拟分页查询
                    List<String> subs = new ArrayList<String>(subSize);
                    for(int i =0 ;i< subSize;i++){
                        subs.add(String.valueOf(i+1));
                    }

                    // 放置到仓库中
                    warehouse.put(startIndex,subs);

                    // 通知mainWatch
                    mainWatch.countDown();

                    // 通知producerWatch
                    producerWatch.countDown();
                }
            });
        }
        producerPools.shutdown();


        // start consumer
        new Thread(new Runnable() {
            @Override
            public void run() {

                SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
                Sheet sheet = workbook.createSheet("测试");

                // producer仍然存活或者还有没消费完成的
                while ( producerWatch.getCount() > 0 ||  !warehouse.isEmpty()){


                    while (!warehouse.isEmpty()){

                        // 写数据到workbook.sheet
                        for (Iterator<Map.Entry<Integer,List<String>>> entryIt = warehouse.entrySet().iterator(); entryIt.hasNext();){

                            Map.Entry<Integer,List<String>> subsEntry = entryIt.next();

                            Integer startIndex = subsEntry.getKey();
                            List<String> subs = subsEntry.getValue();

                            if(subs != null && !subs.isEmpty()){
                                for (int i = 0 ;i < subs.size() ; i ++){
                                    Row row = sheet.createRow(startIndex + i);

                                    row.createCell(0).setCellValue(subs.get(i));
                                }
                            }

                            // remove掉
                            entryIt.remove();
                        }
                    }



                    // 在当前数据以及写入完成后,但是生产者还没有停止,暂时休眠1s,等待生产
                    try {
                        // 休眠1s
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // 写workbook到文件中
                // 如果将这步放到main Thread中,还是会发生出现不可读取内容,?是否还是会出现
                try {
                    OutputStream out = new FileOutputStream("E:\\ps.excel");
                    workbook.write(out);

                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // 通知mainWatch
                mainWatch.countDown();
            }
        }).start();



        // 主线程等待
        try {
            mainWatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Excel complete......");
    }



    private static class Consumer implements Runnable{

        private ConcurrentHashMap<Integer,List<String>> warehouse;


        public Consumer(ConcurrentHashMap<Integer,List<String>> warehouse) {
            this.warehouse = warehouse;
        }

        @Override
        public void run() {




        }
    }



    @Test
    public void testThreadWrite(){

        final CountDownLatch mainWatch = new CountDownLatch(1);

        // start consumer
        new Thread(new Runnable() {
            @Override
            public void run() {

                SXSSFWorkbook workbook = new SXSSFWorkbook(100);
                Sheet sheet = workbook.createSheet("测试");

                List<String> alls = new ArrayList<String>();
                for( int i =0 ;i < 10*1000 ;i++){
                    alls.add(String.valueOf(i));
                }


                for(int i = 0 ;i < alls.size();i++){

                    Row row = sheet.createRow(i+1);

                    row.createCell(0).setCellValue(alls.get(i));
                }
                alls.clear();


                // 写workbook到文件中
                // 如果将这步放到main Thread中,还是会发生出现不可读取内容,?是否还是会出现
                try {
                    OutputStream out = new FileOutputStream("E:\\thread.excel");
                    workbook.write(out);

                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // 通知mainWatch
                mainWatch.countDown();
            }
        }).start();


        try {
            mainWatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Excel complete......");
    }

    @Test
    public  void testSXSSF()  {
        SXSSFWorkbook wb = new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
        Sheet sh = wb.createSheet();
        for(int rownum = 0; rownum < 1000; rownum++){
            Row row = sh.createRow(rownum);
            for(int cellnum = 0; cellnum < 10; cellnum++){
                Cell cell = row.createCell(cellnum);
                String address = new CellReference(cell).formatAsString();
                cell.setCellValue(address);
            }

        }

        // Rows with rownum < 900 are flushed and not accessible
        for(int rownum = 0; rownum < 900; rownum++){
            Assert.assertNull(sh.getRow(rownum));
        }

        // ther last 100 rows are still in memory
        for(int rownum = 900; rownum < 1000; rownum++){
            Assert.assertNotNull(sh.getRow(rownum));
        }


        try {
            FileOutputStream out = new FileOutputStream("E:\\sxssf.excel");
            wb.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // dispose of temporary files backing this workbook on disk
        wb.dispose();
    }
}
