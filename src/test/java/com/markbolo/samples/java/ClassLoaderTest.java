package com.markbolo.samples.java;

import org.junit.Assert;
import org.junit.Test;

/**
 * @auth Administrator
 * @date 2017/7/17
 */
public class ClassLoaderTest {


    @Test
    public void testGetResource(){

        // 普通java项目中classLoader.getResource("/") 返回null
        Assert.assertEquals(null,this.getClass().getClassLoader().getResource("/"));


        System.out.println(this.getClass().getClassLoader().getResource(""));
        System.out.println(this.getClass().getClassLoader().getResource("com/markbolo/samples/java"));
        System.out.println(this.getClass().getClassLoader().getResource("jdbc.properties"));

        // note: getResource() 在test resources中加载不到,会到resources中加载,就会获取到classes的路径,而不是test-classes的路径
        Assert.assertEquals(this.getClass().getClassLoader().getResource("jdbc.properties").getPath(),
                this.getClass().getClassLoader().getResource("").getPath()+"jdbc.properties");

        Assert.assertEquals(this.getClass().getResource("/jdbc.properties").getPath(),
                this.getClass().getResource("/").getPath()+"jdbc.properties");

    }
}
