package com.markbolo.samples.patterns.builder;

import org.junit.Test;

/**
 * @author shenxf
 * @date 2017/8/31
 */
public class BuilderTest {

    @Test
    public void testBuilder(){

        NutritionFacts facts = new NutritionFacts.Builder(10,10)
                .calories(20)
                .fat(10)
                .carbohydrate(30)
                .sodium(25).build();


    }
}
