package com.markbolo.samples.patterns.factory;

import com.markbolo.samples.patterns.factory.method.ChicagoStylePizzaStore;
import com.markbolo.samples.patterns.factory.method.NewYorkStylePizzaStore;
import com.markbolo.samples.patterns.factory.method.PizzaStore;
import com.markbolo.samples.patterns.factory.pizza.Pizza;
import org.junit.Test;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class FactoryMethodTest {

    @Test
    public void testFactoryMethod() throws NoSuchPizzaException {

        PizzaStore newYorkPizzaStore = new NewYorkStylePizzaStore();
        PizzaStore chicagoPizzaStore = new ChicagoStylePizzaStore();

        Pizza pizza = newYorkPizzaStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        pizza = chicagoPizzaStore.orderPizza("cheese");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");
    }
}
