package com.markbolo.samples.patterns.factory;

import com.markbolo.samples.patterns.factory.simple.PizzaStore;
import com.markbolo.samples.patterns.factory.simple.SimpleFactory;
import org.junit.Test;

/**
 * @author shenxf
 * @date 2017/9/4
 */
public class FactoryTest {

    @Test
    public void testSimple() throws NoSuchPizzaException {

        PizzaStore pizzaStore = new PizzaStore(new SimpleFactory());

        pizzaStore.orderPizza("veggie");
    }


}
