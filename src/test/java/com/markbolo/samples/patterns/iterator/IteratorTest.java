package com.markbolo.samples.patterns.iterator;

import org.junit.Test;

/**
 * @author shenxf
 * @date 2017/9/5
 */
public class IteratorTest {


    @Test
    public void testPrintMenu(){
        Waitress waitress = new Waitress(new PancakeHouseMenu(),new DinerMenu());

        waitress.printMenu();
    }
}
