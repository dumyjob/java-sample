package com.markbolo.samples.txt;

import com.markbolo.samples.util.PageHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * @auth Administrator
 * @date 2017/7/27
 */
public class TxtTest {


    private final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private Date startTime;

    private Date endTime;

    @Before
    public void before(){
        startTime = Calendar.getInstance().getTime();
    }

    @After
    public void after(){
        endTime = Calendar.getInstance().getTime();

        System.out.println("时间段: "+ format.format(startTime) +" -- " + format.format(endTime));
        System.out.println("总共用时: "+ (endTime.getTime() - startTime.getTime()));
    }


    @Test
    public void testCompletionService(){

        List<String> result = new ArrayList<String>();

        // 共用线程池，是为了从全局角度，叫多线程可控
        Executor executor = Executors.newCachedThreadPool();

        // 多线程任务管理
        CompletionService<List<String>> completionService = new ExecutorCompletionService<List<String>>(executor);

        int totalCount = 95;
        int pages = 10;
        int pageSize = 10;
        for(int i = 0;i< pages ; i++){

            final int startIndex = PageHelper.start(i,pageSize);
            final int endIndex = PageHelper.end(i,pageSize,totalCount);

            // 根据总记录数count和线程数Server.threadCount进行分页任务分发
            completionService.submit(new Callable<List<String>>() {
                @Override
                public List<String> call() throws Exception {
                    // mock 分页查询
                    // 每个分页查询耗时1s
                    Thread.sleep(1000);

                    List<String> data = new ArrayList<String>();
                    // 每个分页有1000数据
                    for( int i = startIndex ;i < endIndex; i ++ ){
                        // Row
                        data.add("Row "+i+","+(i+1));
                    }

                    return data;
                }
            });
        }


        try {

            for(int i = 0;i < pages; i++){
                result.addAll(completionService.take().get());
            }

            // 检查结果是否是有顺序的
            // 不能保证顺序
            for(String string : result){
                System.out.println(string);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }


    /**
     * 将查询结果写到多个文件中,然后再拼接到一个文件
     */
    @Test
    public void testFile(){

        int totalCount = 95;
        int pages = 10;
        int pageSize = 10;

        ExecutorService pools = Executors.newCachedThreadPool();
        final CountDownLatch mainWatch = new CountDownLatch(pages);

        for(int i = 0;i< pages ; i++){

            final int startIndex = PageHelper.start(i,pageSize);
            final int endIndex = PageHelper.end(i,pageSize,totalCount);

            final int section = i;

            // 根据总记录数count和线程数Server.threadCount进行分页任务分发
            pools.submit(new Runnable() {
                @Override
                public void run() {
                    // mock 分页查询
                    // 每个分页查询耗时1s
                    try {
                        Thread.sleep(1000);

                        List<String> data = new ArrayList<String>();
                        // 每个分页有1000数据
                        for( int i = startIndex ;i < endIndex; i ++ ){
                            // Row
                            data.add("Row "+i+","+(i+1));
                        }


                        // 写数据到分段文件中
                        // 分段文件命名规则 download_${section}_tmp.tmp
                        File secFile = new File("E:\\download_"+ section+"_tmp.tmp");

                        FileWriter fWriter = new FileWriter(secFile);
                        for(String rowData : data){
                            fWriter.write(rowData);
                            fWriter.write("\r\n");
                        }

                        fWriter.flush();
                        fWriter.close();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {

                        // 通知
                        mainWatch.countDown();
                    }
                }
            });
        }


        try {
            mainWatch.await();

            File file = new File("E:\\download.csv");
            if(file.exists() && !file.isDirectory()){
                file.delete();
            }
            file.createNewFile();

            // 写Header
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write("行号");
            writer.write(",");
            writer.write("数据");
            writer.write("\r\n");


            // 将分段文件拼成一个文件
            BufferedReader reader = null;
            for( int i = 0 ;i< pages;i++){

                reader = new BufferedReader(new FileReader(new File("E:\\download_"+i+"_tmp.tmp")));

                char[] buffer = new char[1024];
                int len = 0;
                while((len = reader.read(buffer))  > 0){
                    writer.write(buffer,0,len);
                }
            }

            reader.close();

            writer.flush();
            writer.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
